
declare @date datetime;
set @date=getdate();

select sr.orderid,
sr.[Subject] as [Name],sr.[ReqID], [ProductID],sr.[Created],sr.Due,[JurisID],sr.Status
 ,[dbo].[UDF_Return_OpenOverHours](sr.[Created], @date) as [OpenOverHrs],[dbo].[DATEDIFFEXCWEH](sr.[Created], @date) / 60.0 as [HoursOpen]
into #temp_search_tat
from SearchReq sr 
WHERE sr.[Status] < 100;   

--drop table #temp_search_tat


--with cte_site_setting as
--(select distinct  ActID,sValue from SiteSettings ss (nolock) where sName like '%ConfidentialCompany%'  )


select 
 [Name], 
cg.[GroupName] as [CustomerGroup],          
 a.[Company] as [Account], 
  tsr.[OrderID], 
  tsr.[ReqID], 
  CASE WHEN tsr.[ProductID] = 4         
     THEN '00'          
    WHEN tsr.[ProductID] IN ( 49,  --Multi-State/Jurisdiction Criminal Records Locator          
           77,  --Multi-State/Jurisdiction Criminal Records Locator w/o Sex Offender Search          
           82,  --Multi-State/Jurisdiction Criminal Records Locator (3 year search)          
           83,  --Multi-State/Jurisdiction Criminal Records Locator (5 year search)          
           84,  --Multi-State/Jurisdiction Criminal Records Locator (7 year search)          
           48,  --National Criminal          
           58  --Federal Criminal Extended          
          )        
     THEN 'NW'          
   ELSE j.[JurisRoot]         
  END as [State],          
  CASE WHEN pt.[Type] IN (10,17)         
     THEN 'CIVIL: ' + j.[JurisDesc]          
    WHEN tsr.[ProductID] IN (8, 86)         
     THEN 'STATE OF ' + j.[JurisRoot]          
    WHEN tsr.[ProductID] IN ( 49,  --Multi-State/Jurisdiction Criminal Records Locator          
           77,  --Multi-State/Jurisdiction Criminal Records Locator w/o Sex Offender Search          
           82,  --Multi-State/Jurisdiction Criminal Records Locator (3 year search)          
           83,  --Multi-State/Jurisdiction Criminal Records Locator (5 year search)          
           84,  --Multi-State/Jurisdiction Criminal Records Locator (7 year search)          
           48,  --National Criminal          
           58  --Federal Criminal Extended          
          )        
     THEN 'NATIONWIDE'             
   ELSE j.[JurisDesc]         
  END as [County],
  tsr.[Created],
  tsr.Due, 
  --[dbo].[UDF_Return_OpenOverHours](sr.[Created], @date) as 
  tsr.[OpenOverHrs],          
  --[dbo].[DATEDIFFEXCWEH](sr.[Created], @date) / 60.0 as
   tsr.[HoursOpen],  
  sl.[LookupText] as [CurrentQueue], 

  pt.[ProductCode],    
  --sr.[ProductID],    
  --cp.PODSId,
  cp.[PODSDescription] ,
  case when pt.ProductID in (1,2,8,9,10,39,48,49,58,73,74,75,76,77,82,83,84,85,86,105,123,124,125,126,128,129,135,136,154,178,185,197,198,200,222)
  then 'Core Crim' else 'Non Core'   end OpsProductMapping,RouteType,
  case when sris.StoreStatus in (10,11) then 'Acknowledged by AC'  else 'unknown' end AC_ack_status,ss.sValue
    into ##Tmp_OpenReqsCrim
FROM     bgorders bgo (NOLOCK) 
 --INNER JOIN SearchReq sr (NOLOCK) ON bgo.[BGOrderID] = sr.[OrderID]   --284284  1 min
 inner join #temp_search_tat tsr on bgo.[BGOrderID]=tsr.orderid
 INNER JOIN [ProductTypes] pt (NOLOCK)  ON pt.[ProductID] = tsr.[ProductID]   --284252 41 sec 
 INNER JOIN [Accounts] a (NOLOCK)  ON a.[ActID] = bgo.[ActID] --284284  06 sec
 INNER JOIN [Customers] c (NOLOCK)  ON c.[CustID] = a.[CustID] --284284  33 sec         
 INNER JOIN [CustomerGroups] cg (NOLOCK)  ON cg.[CustGroupID] = c.[CustGroupID]         --284284  38 sec          
 LEFT OUTER JOIN [Juris] j (NOLOCK) ON j.[JurisID] = tsr.[JurisID]   --284284  39 sec          
 LEFT OUTER JOIN [SysLookup] sl (NOLOCK) ON sl.[LookupValue] = tsr.[Status]  AND sl.[Type] = 52  --284284  40 sec          
LEFT OUTER JOIN [CustomerPODs] cp (NOLOCK)  ON bgo.[CustomerPOD] = cp.[PODSId]  -- --284252 40 sec 
left outer join   searchReqImpalaStores sris (nolock) on  tsr.[ReqID]=sris.ReqID  --284252 40 sec 
left outer join SiteSettings ss (nolock) on  ss.ActID=bgo.ActID and sName like '%ConfidentialCompany%'  
WHERE tsr.[Status] < 100 
  --AND sr.[ProductID] <> 154  -- Exclusion of CRSD Product - 09/05/2012' 
  --add confidential tag for those customer
  and pt.ProductID in (1,2,7,8,9,10,12,14,18,37,38,39,40,41,42,45,48,49,50,51,52,56,57,58,68,73,74,75,76,77,79,80,81,82,83,84,85,86,88,92,102,103,104,105,106,116,117,
 118,119,120,123,124,125,126,128,129,134,135,136,146,152,154,156,157,161,162,163,166,167,168,169,170,174,175,177,178,185,186,188,197,198,200,218,222,232)
--and pt.ProductID in ('abuse','abusead','arrdiraud','arrdircdt','arrdircrfm','arrdircrst','bissdoc','bnkliens','businessprf','cbi','cbsv','cdlis','crdoc','credu','cremp','crfed','crfedex',
--'crfm','crfmfd','crfmx','crfmxv','crnat','crnatuo','crnatv','crnatvns','crnatvplus','crsd','crsex','crsexdoj','crsexn','crst','cvcty','cvctyu','cvfed','dea','dirship','dotfmcsa','dpl',
--'dr','dtc','epls','eraqrtly','exoig','exoigb','facis','facisl','facisv','fedbnk','fedprison','ffiea','fngrprn','fptpa','globex','histcivil','idst','ldp','media','mediai','mediav','mediavii',
--'msmjcrfm','msmjcrst','msmjiii','msmjv','msmjvii','njst','npdb','nyomig','ofac','oighcfa','patriot','smschar','smscharplus','smscust','smsrep','ssc1','ssc2','ssc3','tsc1','uccbus','uccindv',
--'udr','vasp','watch','wc')


--Get Vendor Data          
SELECT srt.[ReqID],          
     MAX(srti.[TraceItemID]) as [TraceItemID]           
 INTO ##Tmp_Vendor          
FROM SearchReqTrace srt (NOLOCK)          
 INNER JOIN [SearchReqTraceItems] srti (NOLOCK) ON srti.[TraceID] = srt.[TraceID]          
 INNER JOIN ##Tmp_OpenReqsCrim t ON t.[ReqID] = srt.[ReqID]          
WHERE srti.[ItemType] = 1          
GROUP BY srt.[ReqID]


CREATE INDEX [IX_Tmp_Vendor] ON [##Tmp_Vendor] ([ReqID] ASC)           


          
--Get Researcher Info          
SELECT srt.[ReqID],          
       MAX([TraceItemID]) as [TraceItemID]          
 INTO ##Tmp_Researcher          
FROM [SearchReqTraceItems] srti (nolock)         
 INNER JOIN [SearchReqTrace] srt (NOLOCK) ON srti.[TraceID] = srt.[TraceID]          
 INNER JOIN ##Tmp_OpenReqsCrim t  ON t.[ReqID] = srt.[ReqID]          
WHERE srti.[ItemType] = 20          
GROUP BY srt.[ReqID]          

CREATE INDEX [IX_Tmp_Researcher] ON [##Tmp_Researcher] ( [ReqID] ASC)           

          
--Get Last StatusRecord for each Req          
SELECT srt.[ReqID],          
  MAX([TraceItemID]) as [TraceItemID]          
 INTO ##Tmp_StatusRecord          
FROM [SearchReqTraceItems] srti   (nolock)       
 INNER JOIN [SearchReqTrace] srt (NOLOCK)  ON srti.[TraceID] = srt.[TraceID]          
 INNER JOIN ##Tmp_OpenReqsCrim t ON t.[ReqID] = srt.[ReqID]          
WHERE srti.[ItemType] = 25 --StatusRecord          
GROUP BY srt.[ReqID] 
         
CREATE INDEX [IX_Tmp_StatusRecord] ON [##Tmp_StatusRecord]( [ReqID] ASC) 



alter table ##Tmp_OpenReqsCrim add Vendor varchar(60)
alter table ##Tmp_OpenReqsCrim add [Researcher] varchar(30)
alter table ##Tmp_OpenReqsCrim add [QueueEntryDateTime] datetime
  
--Update Vendor column          
UPDATE c          
  SET c.[Vendor] = crm.[Company]          
FROM ##Tmp_OpenReqsCrim c          
 INNER JOIN ##Tmp_Vendor v ON v.[ReqID] = c.[ReqID]          
 INNER JOIN [SearchReqTraceItems] srti (NOLOCK) ON srti.[TraceItemID] = v.[TraceItemID]          
 INNER JOIN [crmAccounts] crm (NOLOCK) ON crm.[ActID] = srti.[VendorID]  

--Update Researcher column          
UPDATE t          
 SET t.[Researcher] = s.[UserName]          
FROM ##Tmp_OpenReqsCrim t          
 INNER JOIN ##Tmp_Researcher r ON r.[ReqID] = t.[ReqID]          
 INNER JOIN [SearchReqTraceItems] srti (NOLOCK) ON srti.[TraceItemID] = r.[TraceItemID]          
 INNER JOIN [Staff] s (NOLOCK) ON s.[StaffID] = srti.[OwnerRecord]          
 

--Update QueueEntryDateTime column          
UPDATE t          
 SET t.[QueueEntryDateTime] = srti.[Created]          
FROM ##Tmp_OpenReqsCrim t          
 INNER JOIN ##Tmp_StatusRecord r ON r.[ReqID] = t.[ReqID]          
 INNER JOIN [SearchReqTraceItems] srti (NOLOCK) ON srti.[TraceItemID] = r.[TraceItemID]
